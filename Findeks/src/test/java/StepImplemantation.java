import com.thoughtworks.gauge.Step;
import org.junit.Assert;
import java.util.ArrayList;

public class StepImplemantation extends BaseTest {
    @Step("Teb anasayfasina git")
    public void anaSayfayaGit() throws InterruptedException {
        getUrl();
        System.out.println("Anasayfa yuklendi");
    }
    @Step("<int> saniye bekle")
    public void waitFor(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Step("<key> elementine tikla")
    public void clickLogin(String key) {
        clickElement(key);
    }
    @Step("<key> elementinin üzerine gel")
    public void hover(String key) {
        hoverElement(key);
    }
    @Step("<key> elementi var mi")
    public void checkElement(String key) {
        try {
            findElement(key);
        } catch (Exception e) {
            Assert.fail("Element bulunamadi.");
        } }
    @Step("Yeni sekmeye gec")
    public void switchToNewTab() {
        setNewTab(new ArrayList<String>(driver.getWindowHandles()));
        getNewTab().remove(getOldTab());
        driver.switchTo().window(getNewTab().get(0));
    }
    @Step("Sekmeyi kapat")
    public void switchToOldTab() {
        driver.close();
        driver.switchTo().window(getOldTab());
    }
    @Step("<key> elementi ile <text>'i karsilastir")
    public void checkName(String key, String text) {
        assertElementText(key,convertTurkishChar(text));
    }
}
